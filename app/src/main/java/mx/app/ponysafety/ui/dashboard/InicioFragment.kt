package mx.app.ponysafety.ui.dashboard

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Button
import androidx.fragment.app.Fragment
import mx.app.ponysafety.Alarma
import mx.app.ponysafety.R

class InicioFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_inicio, container, false)

        val btn_alerta = root.findViewById<Button>(R.id.btn_alerta)
        btn_alerta?.setOnClickListener {
            val intent = Intent(context, Alarma::class.java)
            startActivity(intent)
        }

        return root
    }


}
